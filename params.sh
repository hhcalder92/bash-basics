#!/bin/bash


usage() { 
    echo "this is the usage function" 1>&2
    echo "Usage: $0 [ -hbrg ] [ -c CUSTOM_COLOR ]" 1>&2
    exit
}

while getopts "hbrgc:" OPTION; do
    case $OPTION in
    h)
        usage
        ;;
    b)
        COLOR=BLUE
        ;;
    r)
        COLOR=RED
        ;;
    g)
        COLOR=GREEN
        ;;
    c)
        COLOR=$OPTARG
        ;;
    *)
        echo "Incorrect options provided"
        exit 1
        ;;
    esac
done

echo "Color is $COLOR"
exit 0;
